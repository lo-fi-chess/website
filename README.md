# website

```sh
docker-compose up --detach
docker-compose exec website yarn serve
```

```sh
docker-compose down --volumes --rmi all --remove-orphans
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
